package ch.vbcmoenchi.backend;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by marcs on 13.08.2017.
 */
@RestController
public class HelloWorldController {

    @RequestMapping("/hello")
    public ResponseEntity<String> sayHello(){
        return new ResponseEntity<String>("Hello world",HttpStatus.OK);
    }
}
