package ch.vbcmoenchi.backend;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by marcs on 13.08.2017.
 */
@RestController
@RequestMapping("/users")
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @GetMapping
    public ResponseEntity<Entity[]> findAll(RestTemplate restTemplate) {
        String allUsers = restTemplate.getForObject("https://vbcm-couchdb.sloppy.zone/vbcm/_all_docs", String.class);
        log.info(allUsers);
        ObjectMapper mapper = new ObjectMapper();
        Entity[] allUsersArray=null;
        try {
            JsonNode treeNode = mapper.readTree(allUsers);
            JsonNode users = treeNode.at("/rows");
            allUsersArray = mapper.treeToValue(users, Entity[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<Entity[]>(allUsersArray, HttpStatus.OK);
    }
    @GetMapping("/{key}")
    public ResponseEntity<User> findOne(RestTemplate restTemplate, @PathVariable("key") String key) {
        User user = restTemplate.getForObject("https://vbcm-couchdb.sloppy.zone/vbcm/"+key, User.class);
        log.info(user.toString());

        return new ResponseEntity<>(user, HttpStatus.OK);
    }
}
