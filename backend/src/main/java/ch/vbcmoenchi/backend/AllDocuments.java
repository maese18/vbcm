package ch.vbcmoenchi.backend;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by marcs on 13.08.2017.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AllDocuments<T> {
    @JsonProperty("total_rows")
    private int totalRows;
    private int offset;
    private T[] rows;
}
