package ch.vbcmoenchi.backend;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Created by marcs on 13.08.2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Entity {
    private String id;
    private String key;
}
